const cacheBuster = require('@mightyplow/eleventy-plugin-cache-buster');
const AssetPath=require("./Utilities/11ty/filters/AssetPathFilter.js")

const fs = require('fs');

//const path = require('path');

const { NODE_ENV = 'production' } = process.env

const isProduction = NODE_ENV === 'production'


module.exports= function(eleventyConfig)
{

    if (isProduction)
    {
        const cacheBusterOptions = { sourceAttributes: {script: 'src'}};
        eleventyConfig.addPlugin(cacheBuster(cacheBusterOptions));
    }

//    eleventyConfig.addFilter('assetPath', AssetPath(__dirname));

//	eleventyConfig.addPassthroughCopy({"dist/*.json":"4clients"},{debug:true});

     eleventyConfig.on('afterBuild', async ()=>{ 
//		console.log("wewewewe");   
//		fs.copyFile("dist/data-tnsql.json","4clients/courtney/data-tnsql.json", err=>{});
//		fs.copyFile("dist/data-tnsqldr.json","4clients/patrick/data-tnsqldr.json", err=>{});
} );



return   {

    dir:{
        input: "src",
        includes: "_includes",
        output: "dist"
        },

        templateFormats: ["njk", "md", "ejs","11ty.js"],


        htmlTemplateEngine: "njk",
        markdownTemplateEngine: "njk"
    



}



}

