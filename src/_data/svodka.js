//import fetch from 'node-fetch';

const { AssetCache } = require("@11ty/eleventy-cache-assets");

const fetch = require('node-fetch');
var Enumerable = require('linq');

const mmq=require('./mmqmodules.json');
const fcra=require('./fluent-crawler.json');
const { RestModuleName, SourceInfoFunc , ModuleNameAndProgrammerFunc  }=require('../../Utilities/utils4svodka.js');



module.exports = async function() 
{


        let asset = new AssetCache("json_svodka");

        // check if the cache is fresh within the last day
        if(asset.isCacheValid("10s")) {
          // return cached data.
          return asset.getCachedValue(); // a promise
        }




const data=await require('../../Utilities/data_from_es.js')('tndr-stored');


var mmqbase=Enumerable.from(mmq);
var dtbase=Enumerable.from(data.aggregations.modules.buckets);
var fcrabase=Enumerable.from(fcra);


var dmmq=mmqbase.select(t=>({Module: t.Modules, Programmer: t.Programmer.trim() }));


var qmmq=ModuleNameAndProgrammerFunc(dmmq,fcrabase);



var _getState=_src=>(_src.match(/^[a-zA-Z]{2}/g)||["??"])[0].toUpperCase();



var  _getBucks=(_prefix,_root)=>_root.reduce((_a,_c,_i)=>({..._a,[`${_prefix}_buck0${_i}`]:_c}),{});


var qmmq10= _tt=> ({ Module: _tt.key, State: _getState(_tt.key)  , Cnt:_tt.doc_count , Length:_tt.by_month.buckets.length, MDate: _tt.MAXD.value , Programmer: qmmq(_tt.key).Programmer   });
var qmmq1= _tt=> ({  ...qmmq10(_tt),   ..._getBucks("w",_tt.by_weeks.buckets), ..._getBucks("m",_tt.by_month.buckets) });


var dt20b=dtbase.select(_t=> qmmq1(_t) );

var _restMname=RestModuleName;


var qmmq2= _tt=> ({ Module: _restMname(_tt.Modules), State: _getState(_tt.Modules)  ,  Programmer:   (_tt.Programmer.match(/^([A-Z])/g)||[""])[0]     });

var tytb=
mmqbase
        .where(t=>dtbase.firstOrDefault(w=>w.key.toLowerCase()==_restMname(t.Modules).toLowerCase())==null)
        .where(x=>(_restMname(x.Modules)))
        .select(qmmq2)
        .distinct(x=>x.Module)       
        ;


        var srcinfo=SourceInfoFunc(mmqbase,fcrabase);



 var _rezz_=tytb.union(dt20b);

 var _rezz_1= _rezz_.select(_m=>({..._m,...srcinfo(_m.Module)}));


 var _rezz_2=_rezz_1.orderBy(t=>t.Module).toArray();

 await asset.save(_rezz_2, "json");
 
 return  _rezz_2;




}