const esbuild = require('esbuild')
const manifestPlugin = require('esbuild-plugin-manifest')

const { NODE_ENV = 'production' } = process.env

const isProduction = NODE_ENV === 'production'

// console.log('NODE_ENV',NODE_ENV , 'process.env ',process.env);

module.exports = class {
  data() {
    return {
      permalink: false,
      eleventyExcludeFromCollections: true
    }
  }

  async render() {
    
    await esbuild.build({
      entryPoints: ['src/js/main.js'],
      bundle: true,
      minify: isProduction,
      outdir: 'dist',
      globalName: 'MyTools',
      sourcemap: !isProduction,
//      banner: { js:'//  ABBA'},
//      plugins: isProduction ? [manifestPlugin({  shortNames:true, filename: "../manifest.json" })]:[],
      target: isProduction ? 'es6' : 'esnext'
    });



  }
}