const MONTS_LONG_NAMES = [ "January", "February", "March", "April", "May", "June",  "July", "August", "September", "October", "November", "December" ];
const MONTS_SHORT_NAMES = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun",  "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];



export { MONTS_LONG_NAMES,MONTS_SHORT_NAMES};
