import  agGrid  from  'ag-grid-enterprise';
import  htmx    from  'htmx.org';

import {MONTS_LONG_NAMES,MONTS_SHORT_NAMES  } from './constants';



function    _formatter(params)
{

    var dateAsString = params.data.MDate;
    if (!dateAsString)  return  "";
    var _rez=new Intl.DateTimeFormat("en-US",{  day:"2-digit",month:"2-digit",year:"2-digit"  }  ).format(new Date(dateAsString));
    return  _rez;
}

function  mdy2Date(_src)
{
  if (!_src)  return  null;
  var {MONTH,DAY,YEAR2}=_src.match(/(?<MONTH>\d+).(?<DAY>\d+).(?<YEAR2>\d{2})/).groups;
  var _m=parseInt(MONTH)-1;
  var _d=parseInt(DAY);
  var _y=parseInt(`20${YEAR2}`);
  var _rez=new Date(_y,_m,_d);
  return  _rez;
}

function  mdy2ymd(_src)
{
  if (!_src)  return  null;
  var _rez=(_src??'').replace(/(\d+).(\d+).(\d+)/,"$3$1$2");
  return  _rez;
}


function    _valGetter(_params)
{
    if (!_params.data)   return  '';
//    var _type=_Period;
    var _stype=(_Period??"").trim();
    var _fld_name=`${_stype}_${_params.column.colId}`;
    if (!_params.data[_fld_name])   return  '';
    var  _rez=_params.data[_fld_name].doc_count;

    _change_headers(_params);

    return  _rez;
}


var  _swi='';
function  _change_headers(_params)
{
  if ((_swi==_Period))  return;
  if ( !_params.data["m_buck00"]) return;

  [0,1,2,3,4].forEach(_x=>_change_clmn_hdr(_params,_x));

  _grid.gridOptions.api.refreshHeader();
  _swi=_Period;
}


function  _change_clmn_hdr(_params,_x)
{
  var _colId=`buck0${_x}`;

  var _stype=(_Period??"").trim();
  var _fld_name=`${_stype}_${_colId}`;

  var _title=daterange2string_short(_params.data[_fld_name].key);
  var _tooltip=daterange2string(_params.data[_fld_name].key);

  _grid.gridOptions.columnApi.getColumn(_colId).colDef.headerName=_title;
  _grid.gridOptions.columnApi.getColumn(_colId).colDef.headerTooltip=_tooltip;

}



function  getRating(_node)
{
//    console.log(_node);   
    var _data=_node.data;
    var _cnts=(_data && _data.buck00) ? [_data.buck00,_data.buck01,_data.buck02,_data.buck03,_data.buck04].map(z=>z.doc_count??0) : [0];

    var _type=_Period;
    var _stype=(_type??"").trim();


    _cnts=valueGetter4Dgrm(_node, /* "m" */  _stype );

//    _cnts=_cnts.reverse();
    var _rez=_cnts.reduce((_prev,_curr)=>10*_prev+(_curr[1]>0?1:0),0);
    _rez=_rez.toString().padStart(5, "0")
    return _rez  ;
}



function    _tooltipGetter(_params)
{

  var _type=_Period;
  var _stype=(_type??"").trim();



  var _fld_name=`${_stype}_${_params.column.colId}`;


    if(!_params.data[_fld_name]) return null;

//    console.log('ZZZZ  ',_params.data[_params.column.colId]);

    var _rt=_params.data[_fld_name];

    var _from= (!_rt.from)? "*" : new Date(_rt.from).toLocaleDateString();
    var _to=new Date(_rt.to).toLocaleDateString();

    var  _rez=`${_from}-${_to}`;

    var _title=daterange2string(_rt.key);

//  return _rez;

     return _title;

}



    var   _props_={valueGetter: _valGetter,type:'numericColumn', tooltipValueGetter: _tooltipGetter,minWidth: 50,  sortable: true};


    var  _comparator_ = (a, b) => {
                    const valA = parseInt(a);
                    const valB = parseInt(b);
                    if (valA === valB) return 0;
                    return valA > valB ? 1 : -1;
                };

    var  _comparator_4_sort = (valueA, valueB, nodeA, nodeB, isInverted) => {

                    var ratA=getRating(nodeA);
                    var ratB=getRating(nodeB);

//                    ratA=ratA.split("").reverse().join("");
//                    ratB=ratB.split("").reverse().join("");

                    const valA = parseInt(ratA);
                    const valB = parseInt(ratB);

//                    console.log("valA>>",ratA,"   valB>>",ratB);

                    if (valA === valB) return 0;
                    return valA > valB ? 1 : -1;



                };


    var  _comparator_4_activity_filter = (valAsDate, valAsString) => {

        var _date=mdy2Date(valAsString);
        if (_date==null)  return -1;

        var _sdate=new Intl.DateTimeFormat("en-US",{  day:"2-digit",month:"2-digit",year:"2-digit"  }  ).format(_date);
        var _sdate2=new Intl.DateTimeFormat("en-US",{  day:"2-digit",month:"2-digit",year:"2-digit"  }  ).format(valAsDate);


//        console.log(valAsString,"  <>  ",_sdate,"   <>  ",_sdate2);

        //if (_sdate==_sdate2)  return  1;

        if (_date < valAsDate) 
        {
          return -1;
        } 
        else if (_date > valAsDate) 
        {
          return 1;
        }

        return 0;

    };



    var  _comparator_4_activity_sort = (valA, valB) => {

        var _dateA=mdy2Date(valA);
        var _dateB=mdy2Date(valB);

        if (_dateA==null&&_dateB==null) return  0;

        if (_dateA==null) return  1;
        if (_dateB==null) return  -1;



        var _sdateA=new Intl.DateTimeFormat("en-US",{  day:"2-digit",month:"2-digit",year:"2-digit"  }  ).format(_dateA);
        var _sdateB=new Intl.DateTimeFormat("en-US",{  day:"2-digit",month:"2-digit",year:"2-digit"  }  ).format(_dateB);


//        console.log(valAsString,"  <>  ",_sdate,"   <>  ",_sdate2);

        //if (_sdate==_sdate2)  return  1;

        if (_dateB < _dateA) 
        {
          return -1;
        } 
        else if (_dateB > _dateA) 
        {
          return 1;
        }

        return 0;

    };




    var  _comparator_4_activity_sort_M = (valA, valB) => {

        var _dateA=mdy2ymd(valA);
        var _dateB=mdy2ymd(valB);

        if (_dateB < _dateA) 
        {
          return -1;
        } 
        else if (_dateB > _dateA) 
        {
          return 1;
        }

        return 0;

    };



function  _preo(_src)
{

//  return  _src;

  var _nmax=3;

  var _vals=_src.map(_x=>_x[1]).filter(_x=>_x>0);
  if (_vals.length==0)  return  _src;

  var _max=Math.max(..._vals);

  var _scale=_x=>
  {
    if (_x==0)  return  0;
    if (_x==_max) return  _nmax;
    var _tmp=(_x*_nmax)/_max;
    if (_tmp>0 && _tmp<1) return  1;
    return  Math.round(_tmp);
  };

  var _rez=_src.map(t=>[t[0],_scale(t[1])]);

  return  _rez;

}


function valueGetter4Dgrm(_src,_type)
{  

    var _stype=(_Period??"").trim();
    var _pream= _stype ? `${_stype}_buck0`:"buck0";
    var _data=_src.data;
    var _cond=_data && _data[`${_pream}0`];
    if (!_cond) return  [];
    var _rez=[0,1,2,3,4].map(_i=>[_data[`${_pream}${_i}`].key,_data[`${_pream}${_i}`].doc_count]);

    if (_bars_is_scaled)
    {
      _rez=_preo(_rez);
    }

    return  _rez;
}


function  daterange2string(_src)
{

  var months = MONTS_LONG_NAMES;
  var smonths = MONTS_SHORT_NAMES;

  var  mName=_src=>
  {
    var imonth=parseInt(_src);
    return  smonths[imonth-1];
  };

  _src=`${_src}`;

  var _dates=_src.match(/\d{4}.\d{2}.\d{2}/g);

  var _rgx_2=/(?<DATA>(?<YEAR4>\d{2}(?<YEAR2>\d{2})).(?<MONTH>\d+).(?<DAY>\d+))/;

  var _start=_dates[0].match(_rgx_2).groups;
  var _end=_dates[1].match(_rgx_2).groups;

//  var _groups=xValue.match(/(?<DATA>(?<YEAR>\d+).(?<MONTH>\d+).(?<DAY>\d+))/)

  var _title=_src;

  if (_start.DAY==_end.DAY)
  {
      _title=`${mName(_start.MONTH)} ${_start.YEAR2}`
  }
  else if (_start.YEAR2 != _end.YEAR2)
  {
      _title= `${_start.DAY} ${mName(_start.MONTH)} ${_start.YEAR2} - ${_end.DAY} ${mName(_end.MONTH)} ${_end.YEAR2}`;
  }
  else if (_start.MONTH != _end.MONTH)
  {
      _title= `${_start.DAY} ${mName(_start.MONTH)}-${_end.DAY} ${mName(_end.MONTH)} ${_start.YEAR2}`;
  }
  else
  {
      _title= `${_start.DAY}-${_end.DAY} ${mName(_end.MONTH)} ${_start.YEAR2}`;
  }

  return  _title;

}


function  daterange2string_short(_src)
{


  var months = MONTS_LONG_NAMES;
  var smonths = MONTS_SHORT_NAMES;


  var  mName=_src=>
  {
    var imonth=parseInt(_src);
    return  smonths[imonth-1];
  };

  _src=`${_src}`;

  var _dates=_src.match(/\d{4}.\d{2}.\d{2}/g);

  var _rgx_2=/(?<DATA>(?<YEAR4>\d{2}(?<YEAR2>\d{2})).(?<MONTH>\d+).(?<DAY>\d+))/;

  var _start=_dates[0].match(_rgx_2).groups;
  var _end=_dates[1].match(_rgx_2).groups;

//  var _groups=xValue.match(/(?<DATA>(?<YEAR>\d+).(?<MONTH>\d+).(?<DAY>\d+))/)

  var _title=_src;

  if (_start.DAY==_end.DAY)
  {
      _title=`${mName(_start.MONTH)} ${_start.YEAR2}`
  }
  else if (_start.YEAR2 != _end.YEAR2)
  {
      _title= `${_start.DAY} ${mName(_start.MONTH)} - ${_end.DAY} ${mName(_end.MONTH)}`;
  }
  else if (_start.MONTH != _end.MONTH)
  {
      _title= `${_start.DAY} ${mName(_start.MONTH)}-${_end.DAY} ${mName(_end.MONTH)}`;
  }
  else
  {
      _title= `${_start.DAY}-${_end.DAY} ${mName(_end.MONTH)}`;
  }

  return  _title;

}



function tooltipRenderer(params) 
{
//  console.log("###  ",params);
  const { yValue, xValue } = params;
//  console.log( `${xValue}`     ,"   <>  ",yValue);
  var title=daterange2string(xValue);

  if (!_bars_is_scaled)
  {
    return  { title };
  }


  var _key=`${xValue}`;

  var  content=Object.entries(params.context.data).filter(t=>t[1].key&&t[1].key==_key)[0][1].doc_count;

//  console.log(content);


  return { title, content };
}


function getColumnDefs()
{
    var _columnDefs = [
      { field: "Module", filter: 'agSetColumnFilter', sortable: true },
      { field: "State" , filter: 'agSetColumnFilter', sortable: true},
      { field: "Programmer" , filter: 'agSetColumnFilter', sortable: true},
      { field: "_src" , headerName: "Source" , filter: 'agSetColumnFilter'},
      { field: "_format" , headerName: "Format" ,filter: 'agSetColumnFilter'},

      { 
        field: "MDate",  
        headerName: "Last Activity"   , 
        /* type: ['dateColumn', 'nonEditableColumn'] , */ 
//        valueGetter: params=>{ console.log(params);   return params.data?.MDate??"".split("T")[0];},
        valueGetter: _formatter,
        cellClass: 'dateUS',
        valueFormatter: _formatter , 
        filter: 'agDateColumnFilter',  
        sortable: true, 
        comparator: _comparator_4_activity_sort_M,
        filterParams:{  browserDatePicker:true, 
                        buttons:["clear","reset"], 
                        includeBlanksInLessThan:true,
                        comparator: _comparator_4_activity_filter,
                      } 
      },

        {     headerName: 'Activity rating', type:'numericColumn', sortable: true,  hide:false, valueGetter: getRating , filter: 'agSetColumnFilter'  }  ,

        {     
            headerName:  `Activity by ${(_Period=='w')?'weeks':'months'}` , sortable: true, hide:true,
            children: [

            {   field:"diagram",  headerName: 'Diagram', sortable: true,  hide:false, 
                comparator: _comparator_4_sort,
                filter: 'agSetColumnFilter' ,
        
                valueGetter: xyz=>valueGetter4Dgrm(xyz,  _Period),   
                cellRenderer: 'agSparklineCellRenderer',
                cellRendererParams:{  sparklineOptions: { type: 'column', tooltip:{ enabled:true, renderer:tooltipRenderer} }   },


                filterParams: {
                comparator: _comparator_,
//                textFormatter: (ppp) =>  { console.log("ppXp>> ",ppp); return "777";  },
                valueGetter:  getRating,        // (zaza)=>{console.log("ZAZA>>> ",zaza); return "xyz"; },
//                valueFormatter: (ppp) =>  { console.log("pppZ>> ",ppp); return "888"; return   ppp.value.toString().padStart(5, "0"); }
                }

          }  ,


      { field: "buck00", headerName: '4th ago' , ..._props_ },
      { field: "buck01", headerName: '3rd ago' , ..._props_ },
      { field: "buck02", headerName: '2nd ago' , ..._props_ },
      { field: "buck03", headerName: '1st ago' , ..._props_ },
      { field: "buck04", headerName: 'Current' , ..._props_ },
            ]
        },



    ];

    return  _columnDefs;

}


function  getGridOptions()
{
    // let the grid know which columns to use
    var _gridOptions = {
      columnDefs: /* columnDefs, */  getColumnDefs(),
      defaultColDef: 
      {
        flex: 1,
        minWidth: 50,
        resizable: true,
        floatingFilter: true,
        filterParams:{ browserDatePicker:true, buttons:["clear","reset"]}
      },
      excelStyles:
      [
        {
          id: 'dateUS',
          dataType: 'DateTime',
//          dataType: 'numberType',
          numberFormat: { format: 'mm/dd/yy' }
        }        
      ],
//    rowData:goga,
// rowHeight: 95,
statusBar: {
    statusPanels: [
      { statusPanel: 'agTotalAndFilteredRowCountComponent', align: 'left' },
      { statusPanel: 'agTotalRowCountComponent', align: 'center' },
      { statusPanel: 'agFilteredRowCountComponent' },
      { statusPanel: 'agSelectedRowCountComponent' },
      { statusPanel: 'agAggregationComponent' },
    ],
  },

onFirstDataRendered: params=>params.columnApi.autoSizeAllColumns(),


    };

    return  _gridOptions;

}



function  makeGrid()
{
    var gridDiv = document.querySelector('#myGrid');

    var _go=getGridOptions();
    _go=getGridOptions();

    _grid=new agGrid.Grid(gridDiv, getGridOptions() );


//    fetch('./data-tnsqldr.json')
    fetch(_filename)
    .then(function (response) { return response.json(); })
    .then(function (data) {   _grid.gridOptions.api.setRowData(data); });


    htmx.on("#bmonth", "click", function(evt){ Set_M(evt); });
    htmx.on("#bweek", "click", function(evt){ Set_W(evt); });

    htmx.on("#first2bars", "click", function(evt){ Set_1st2bars(evt); });
    htmx.on("#last2bars", "click", function(evt){ Set_last2bars(evt); });
    htmx.on("#clear2bars", "click", function(evt){ Clear_bars(evt); });

    htmx.on("#export2excel", "click", ExportExcel  );
    htmx.on("#export2csv", "click", ExportCSV  );

    htmx.on("#scale", "click", ScaleBars  );

}





function  Set_Period(_src)
{
  _Period=_src;
  _grid.gridOptions.api.setColumnDefs(getColumnDefs());

  _grid.gridOptions.api.redrawRows();
  _grid.gridOptions.columnApi.applyColumnState({ state: [{ colId: 'Module', sort: 'asc' }], defaultState: { sort: null }  });

  _grid.gridOptions.columnApi.autoSizeAllColumns();
}

function  Set_W(_evnt)
{
  Set_Period("w");
}

function  Set_M(_evnt)
{
  Set_Period("m");
}


function  ExportExcel(_evnt)
{
    _grid.gridOptions.api.exportDataAsExcel({ skipColumnGroupHeaders:true});
}

function  ExportCSV(_evnt)
{
    _grid.gridOptions.api.exportDataAsCsv({ skipColumnGroupHeaders:true});
}

function  Set_Bars(_src)
{
  var hardcodedFilter = {
    diagram: {
      type: 'set',
      values: _src,
    }
  };

  _grid.gridOptions.api.setFilterModel(hardcodedFilter);  
}

const  Set_1st2bars=(_evnt)=>Set_Bars(['10000', '01000','11000']);

const  Set_last2bars=(_evnt)=>Set_Bars(['00001', '00010','00011']);

const  Clear_bars=(_evnt)=>{    
//  console.log("clear bars");

 const instance = _grid.gridOptions.api.getFilterInstance('diagram');

  instance.resetFilterValues();
  instance.setModel(null);
  _grid.gridOptions.api.onFilterChanged();


//_grid.gridOptions.api.getFilterInstance('diagram').resetFilterValues();

};


var   _bars_is_scaled=true;
function  ScaleBars(_evnt)
{
  _bars_is_scaled=document.querySelector('#scale').checked;
  _grid.gridOptions.api.redrawRows();
}




var _Period="w";
var _grid;


var _filename;

function    LoadData(_src)
{
    _filename=_src;
//    document.addEventListener('DOMContentLoaded', makeGrid );
    makeGrid();
}

// return  {LoadData};

//window.loada=LoadData;

export   {LoadData,_grid} ;