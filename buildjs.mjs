import { GridOptionsWrapper } from 'ag-grid-community';
import { build } from 'esbuild';
//import { skypackResolver } from 'esbuild-skypack-resolver';

build({
    entryPoints: ['./src/js/main.js'],
    bundle: true,
//    minify: true,
    outfile: './dist/main.js',
    globalName: 'MyTools',
//    format: 'esm',
//    external:[ "ag-grid-community"],
//    plugins: [skypackResolver()],

}).catch(() => process.exit(1));
