const fs = require('fs');
const path = require('path');

const { NODE_ENV = 'production' } = process.env;

const isProduction = NODE_ENV === 'production';


module.exports=function(_path)
{

var ttt=function(value) 
{
    //    if ((process.env.ELEVENTY_ENV === 'production')) 
        if (isProduction) 
        {
          const manifestPath = path.resolve(
              _path,
    //        __dirname,
    //        "dist",
            'manifest.json'
          );
          const manifest = JSON.parse(fs.readFileSync(manifestPath));
    //      console.log(manifest);
          return `${manifest[value]}`;
        }
        return `${value}`;
};

return  ttt;

}


/*
    <script src="{{ 'main.js' | assetPath }}"></script>
*/