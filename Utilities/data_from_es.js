const fetch = require('node-fetch');

module.exports = async function(_indx) 
{
    const by_weeks=
    {
      "by_weeks": {
        "range": {
          "field": "import_date",
          "keyed":false,
          "ranges":
          [
            {"from":"now-4w/w","to":"now-3w/w"},
            {"from":"now-3w/w","to":"now-2w/w"},
            {"from":"now-2w/w","to":"now-1w/w"},
            {"from":"now-1w/w","to":"now/w"},
            {"from":"now/w","to":"now+1w/w"}
          ]
        }
      }    
    };
 
  const by_month=
  {
    "by_month": {
      "range": {
        "field": "import_date",
        "keyed":false,
        "ranges":
        [
          {"from":"now-4M/M","to":"now-3M/M"},
          {"from":"now-3M/M","to":"now-2M/M"},
          {"from":"now-2M/M","to":"now-1M/M"},
          {"from":"now-1M/M","to":"now/M"},
          {"from":"now/M","to":"now+1M/M"}
        ]
      }
    }    
  };
  
  
    const MAXD=
    {
      "MAXD": {  "max": { "field": "import_date"   }   }
    };
  
      const body = { 
          size: 0,
          aggs:{
              modules:{
                  terms:{
                      field: "type.keyword",
                      order: { _key: "asc"   },
                      size: 1000
                    },
      
                    "aggs": {  ...MAXD ,  ...by_month , ...by_weeks  }              
      
              }
          }
      
      };
      
  
      // console.log(body);


//	const es_root='http://elasticsearch0.ngrok.io';


	const es_root_='https://cdfd-68-187-225-254.ngrok.io';
  

      const response = await fetch(`${es_root_}/${_indx}-*/_search`,{ method:"POST", body: JSON.stringify(body), headers: {'Content-Type': 'application/json'}  });
      const data = await response.json();
  
      return    data;
      
}