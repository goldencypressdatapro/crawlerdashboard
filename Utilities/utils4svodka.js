var Enumerable = require('linq');


const RestModuleName=_src=>{

    var _name=_src.trim();
  
    if (_name.match(/^[^\n]+?Module$/i))
    {
      return  _name;
    }
  
    if (!_name.match(/^[^\n]+$/i))
    {
      return  "";
    }
  
  
    return  `${_name}Module`;
  };
  

function SourceInfoFunc(mmqbase,fcrabase)
{

    var srcinfo=_namemod=>{


        var _name_1=_namemod.toLowerCase();
        var _name_2=(_namemod.match(/^(.+?)Module\s*$/)||["?","?"])[1].toLowerCase();
        var _arr_names=[_name_1,_name_2];
      
        var _rez=mmqbase.firstOrDefault(t=>_arr_names.includes(t.Modules.toLowerCase()));
      
        if(_rez)
        {
          var _cond_1=_rez['Source/Weblink'].match(/file.+drop/ig)!=null;
          var _cond_2=_rez.Format.match(/file.+drop/ig)!=null;
          var _cond = _cond_1||_cond_2;
          var  _src=_cond ? "Local" : "Web";
          var  _format0=(_rez.Format.match(/csv|Excel|pdf/ig)||["html"])[0];
          var _format=`${_format0[0].toUpperCase()}${_format0.substring(1).toLowerCase()}`;
          return  {_src:_src,_format:_format};
        }
      
      
        var _rez0=fcrabase.firstOrDefault(t=>_arr_names.includes(t.name.toLowerCase()));
        if(_rez0)
        {
          var  _src= [..._rez0.sourceTypes,"xxx"][0];
          var _format= [..._rez0.fileTypes,"xxx"][0];
          return  {_src:_src,_format:_format};
        }
      
      
        
      
        return  {};
      
      };
      
      return  srcinfo;


}


function  ModuleNameAndProgrammerFunc(dmmq,fcrabase, _programmer="")
{
  var qmmq=_namemod=>{

    var _rez=dmmq.firstOrDefault(t=>t.Module.toLowerCase()==_namemod.toLowerCase());
  
    if (!_rez)
    {
      var _mod0=(_namemod.match(/^(.+?)Module\s*$/)||["?","?"])[1].toLowerCase();
      _rez=dmmq.firstOrDefault(t=>t.Module.toLowerCase()==_mod0);
    }
  
    if ( _programmer && (!_rez) )
    {
      var _mod1=fcrabase.firstOrDefault(t=>(t.name.toLowerCase()==_namemod.toLowerCase()));
      if (_mod1)
      {
          _rez={Module:_namemod,Programmer:"I"};
      }
    }


    _rez=_rez||{Module:_namemod,Programmer:""};
  
    _rezm={ Module: _rez.Module, Programmer: (_rez.Programmer.match(/^([A-Z])/g)||[""])[0] }

    return  _rezm;
  
  };

  return  qmmq;
}



var _normDate=_src=>{
  if (!_src)  return  "";
  //console.log(`>>>>${_src}<<<<`)
  var _m=_src.match(/(\d{4})-(\d{2})-(\d{2})T/);
  if (_m==null)
  {
    return  "";
  } 

  var _rez=`${_m[3]}/${_m[2]}/${_m[1]}`;

  return  _rez;

};


var  _normdate=_src=>new Intl.DateTimeFormat("en-US",{  day:"2-digit",month:"2-digit",year:"2-digit"  }  ).format(new Date(_src));











  module.exports= { RestModuleName , SourceInfoFunc , ModuleNameAndProgrammerFunc } ;